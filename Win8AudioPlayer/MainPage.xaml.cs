﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Media;
using Windows.UI.Notifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace Win8AudioPlayer
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page, INotifyPropertyChanged
    {
        private Uri _selectedItem = null;
        private List<Uri> _items = null;

        public Uri SelectedItem
        {
            get { return this._selectedItem; }
            set
            {
                if (this._selectedItem != value)
                {
                    this._selectedItem = value;
                    this.OnPropertyChanged("SelectedItem");
                }
            }
        }

        public List<Uri> Items
        {
            get { return this._items; }
            set
            {
                if (this._items != value)
                {
                    this._items = value;
                    this.OnPropertyChanged("Items");
                }
            }
        }

        public MainPage()
        {
            this.InitializeComponent();

            this.DataContext = this;
        }

        private void Page_Loaded_1(object sender, RoutedEventArgs e)
        {
            var items = new List<Uri>();
            items.Add(new Uri("http://mediafiles.allaboutwindowsphone.com/aawp_20120801_033.mp3", UriKind.Absolute));
            items.Add(new Uri("http://mediafiles.allaboutwindowsphone.com/aawp_20120726_032.mp3", UriKind.Absolute));
            items.Add(new Uri("http://mediafiles.allaboutwindowsphone.com/aawp_20120718_031.mp3", UriKind.Absolute));
            items.Add(new Uri("http://mediafiles.allaboutwindowsphone.com/aawp_20120709_030.mp3", UriKind.Absolute));

            this.Items = items;

            this.SelectedItem = this.Items[0];
        }

        private void OnPropertyChanged(string propertyName)
        {
            if (this.PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        public event PropertyChangedEventHandler PropertyChanged;
    }
}
