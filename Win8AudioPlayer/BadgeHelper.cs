﻿using Windows.UI.Notifications;

namespace Win8AudioPlayer
{
    public enum Glyphs
    {
        none,
        activity,
        alert,
        available,
        away,
        busy,
        newMessage,
        paused,
        playing,
        unavailable,
        error,
        attention,
    }

    public class BadgeHelper
    {
        public static void UpdateBadge(Glyphs glyph)
        {
            // Get badge xml content 
            var content = BadgeUpdateManager.GetTemplateContent(Windows.UI.Notifications.BadgeTemplateType.BadgeGlyph);
            var element = content.GetElementsByTagName("badge")[0];

            // Set new glyph value
            element.Attributes[0].NodeValue = glyph.ToString();

            // Update badge
            var notification = new BadgeNotification(content);
            BadgeUpdateManager.CreateBadgeUpdaterForApplication().Update(notification);
        }
    }
}
